# January 2015
#
# Function to test and use TI Sensor Tag in python without resorting
# to hacks. Needs pygattlib (available on PyPi at
# https://bitbucket.org/OscarAcena/pygattlib).
#
# This software is under the terms of GPLv3 or later.
#
# Code by Mandar Harshe @ LORIA, Nancy, France
# (mandar.harshe@inria.fr, mandarharshe@gmail.com)

# Updates from June 2015 to add in SensorTag 2.0 sensors

from __future__ import print_function
import gattlib
import struct

class Battery(gattlib.GATTResponse, object):
    '''Class for managing the battery service.

    The definition of this closely follows that of Sensors. But we
    stick to using UUIDs (since they are defined in Bluetooth Core
    Spec)

    '''
    def __init__(self, periph):
        gattlib.GATTResponse.__init__(self)
        self.periph = periph
        self.sensor_name = "Battery"
        self.uuid = "2A19"
        self.var_names = ["%", "", ""]
        self.vals = [0, 0, 0]
    def on_response(self, response_string):
        '''Method modified from the GATTResponse super class

        '''
        try:
            self.vals = [struct.unpack('b', response_string)[0], 0, 0]
            self.periph.publish(self)
        except Exception as expt:
            self.periph.printerr(expt)
            raise

class Sensors(gattlib.GATTResponse, object):
    '''Generic Class for Sensors

    Generic Class containing basica enable/disable methods for
    sensors. Constructor takes in reference to parent Tag device. A
    :class:`Sensors` object is always defined as a component of a
    :class:`SensorTag` object.

    .. attribute:: read_time

    Used primarily for storing the last time the sensor was
    read. Useful only when setting up timers with durations longer
    than default maximum measurement period of 2.55 seconds.

    '''
    sensorOn = str(bytearray([1]))
    sensorOff = str(bytearray([0]))
    notifyOn = str(bytearray([1, 0]))
    notifyOff = str(bytearray([0, 0]))

    def __init__(self, periph, characteristic_services=None):
        gattlib.GATTResponse.__init__(self)
        self.periph = periph
        self.handles = dict({'switch': None, \
                             'read': None, \
                             'mes_period': None, \
                             'notify': None})
        self.status = False
        self.sensor_name = None
        self.var_names = ["", "", ""]
        self.vals = [0, 0, 0]
        self.read_time = 0
        self.notify_status = None

    def parse_sensor_service(self, characteristic_services, read, switch, period):
        '''Function assign handles correctly

        This function reads in all available services for the given
        parent service, and assigns the correct read, switch, notify
        and period configuration handles for the sensor services. The
        read, switch, period handles are identified by their short
        uuid (16 bit, ignoring the rest of the common 112 bits)

        :param characteristic_services: The list of all services available.

        :param read: The short 16 bit UUID corresponding to the sensor
         value to be read

        :param switch: The short 16 bit UUID corresponding to the
         service that enables switching the sensor on/off. If the
         notify property is available, the notify handle is generally
         the *next* handle.

        :param period: The short 16 bit UUID to the service that is
         used to configure the sensor period

        '''
        for service in characteristic_services:
            if service.uuid[4:8] == read:
                if ord(service.properties) & 0b00000010:
                    self.handles['read'] = service.val_handle
                #check if notifications are allowed
                if ord(service.properties) & 0b10000:
                    self.handles['notify'] = service.val_handle+1
            elif service.uuid[4:8] == switch:
                self.handles['switch'] = service.val_handle
            elif service.uuid[4:8] == period:
                self.handles['mes_period'] = service.val_handle

    def on_response(self, response_string):
        '''Method modified from the GATTResponse super class

        This method calls the :py:meth:`parse_value` method to parse
        the response string received on calling the
        :meth:`read_by_handle_async` and then calls the
        :meth:`SensorTag.SensorTag.publish` to further *publish* the
        sensor values.

        '''
        try:
            self.vals = self.parse_value(response_string)
            #print(self.vals)
            #print(response_string)
            self.periph.publish(self)
        except Exception as expt:
            print(expt)
            raise

    def parse_value(self, response_string):
        ''' Dummy function'''
        return response_string

    def enable(self):
        '''Method to enable sensor on the Tag.

        '''
        if self.handles['switch'] is not None:
            try:
                resp = self.periph.write_by_handle(self.handles['switch'], self.sensorOn)
                if resp[0] == '\x13':
                    return True
                else:
                    return False
            except:
                raise
        else:
            return False

    def enable_notify(self):
        '''Method to enable sensor notifications on the Tag.

        '''
        if self.handles['notify'] is not None:
            try:
                self.periph.write_by_handle(self.handles['notify'], self.notifyOn)
            except Exception:
                self.periph.printerr("Could not enable notify in " + self.sensor_name)
                raise
            else:
                return True
    def calibrate_vals(self):
        '''Dummy function for calibration.

        '''
        return True

    def disable(self):
        '''Method to disable sensor on the Tag.

        '''
        if self.handles['switch'] is not None:
            try:
                self.periph.write_by_handle(self.handles['switch'], self.sensorOff)
            except Exception:
                # Exception means connection has been lost. we assume
                # sensor to be off in that case
                self.status = False
                raise
            return False

    def disable_notify(self):
        '''Method to disable sensor notifications on the Tag.

        '''
        if self.handles['notify'] is not None:
            try:
                self.periph.write_by_handle(self.handles['notify'], self.notifyOff)
            except Exception:
                self.periph.printerr("Could not disable notify in " + self.sensor_name)
                raise
            else:
                return False

    def set_period(self, time_milli_sec):
        '''Set period of measurements in milliseconds.

        Max allowed is 2.55 seconds. Minimum varies according to
        sensor, generally 100ms

        '''
        if time_milli_sec < 100 or time_milli_sec > 2550:
            print("period out of range, setting to default")
            time_milli_sec = 1000
        time_raw = str(bytearray([time_milli_sec/10]))
        if self.handles['mes_period'] is not None:
            try:
                self.periph.write_by_handle(self.handles['mes_period'], time_raw)
            except Exception as expt:
                print(expt)
                raise

class Accelerometer(Sensors):
    '''Accelerometer object. This class will store the write handles for
    the accelerometer, to switch on/off and change time_period.

    '''
    def __init__(self, periph, characteristic_services):
        Sensors.__init__(self, periph)
        self.sensor_name = "accelerometer"
        self.var_names = ["x axis", "y axis", "z axis"]
        self.parse_sensor_service(characteristic_services, 'aa11', 'aa12', 'aa13')

    def parse_value(self, response_text):
        '''Parse values obtained from Sensor

        :param response_text: The response string sent by sensor with raw data

        '''
        try:
            raw_accls = struct.unpack('bbb', response_text)
            accls = [self.raw_to_float(raw) for raw in raw_accls]
        except ValueError:
            return [0, 0, 0]
        else:
            return accls

    def raw_to_float(self, intVal):
        '''Convert raw values to float.

        The sensor stores 0 - 2g accelerations as values 0-128, and
        values -2g to 0 as values 129 to 255. This function converts
        the integer value to the correct float value (multiplying by
        the value of g = 9.81 m/s^2)

        :param intVal: The raw integer value obtained from sensor
         (between -127 and 128)

        '''
        gravity = 9.81
        r_g = 8 # +/- 2g
        half = 128 # necessary to fix the break-off point for +ve and
                    # -ve readings
        quart = float(half/r_g) # gets the correct unit of reading =
                                  # (1/quart)g
        floatVal = (intVal)/quart
        return floatVal*gravity

class Magnetometer(Sensors):
    '''
    Magnetometer object. This class will store the write handles for
    the magnetometer, to switch on/off and change time_period.
    '''
    def __init__(self, periph, characteristics):
        Sensors.__init__(self, periph)
        self.sensor_name = "magnetometer"
        self.var_names = ["x axis", "y axis", "z axis"]
        self.parse_sensor_service(characteristics, 'aa31', 'aa32', 'aa33')

    def parse_value(self, response_text):
        '''function to convert response text human readable sensor values

        Data is stored as magnetic field around X, Y, Z in little
         endian format. Use :py:func:`struct.unpack` from default
         libraries to parse this to signed integer values. The convert
         to float.

        :param response_text: The response string sent by sensor with
         raw data

        '''
        try:
            intvals = struct.unpack('<hhh', response_text)
            mag = [self.raw_to_float(v) for v in intvals]
        except Exception as expt:
            self.periph.printerr(expt)
            return [0, 0, 0]
        else:
            return mag

    def raw_to_float(self, intvalue):
        '''Convert integer value to float depending on sensor range

        '''
        f_val = intvalue*(2000.0)/pow(2, 16)
        return f_val

class Gyroscope(Sensors):
    '''
    Gyroscope object. This class will store the write handles for
    the gyroscope, to switch on/off and functions to read it.
    '''
    def __init__(self, periph, characteristic_services):
        Sensors.__init__(self, periph)
        self.sensor_name = "gyroscope"
        self.var_names = ["x axis", "y axis", "z axis"]
        self.parse_sensor_service(characteristic_services, 'aa51',
                                  'aa52', 'aa53')

    def parse_value(self, response_text):
        '''function to convert response text human readable sensor values

        :param response_text: The response string sent by sensor with
         raw data

        '''
        try:
            gyraw = struct.unpack('<hhh', response_text)
            gyr = [self.raw_to_float(raw) for raw in gyraw]
        except:
            return [0, 0, 0]
        else:
            return gyr

    def raw_to_float(self, int_val):
        '''Convert integer value to float depending on sensor range

        '''
        f_val = int_val*(500.0)/pow(2, 16)
        return f_val


class Humidity(Sensors):
    '''Humidity object. This class will store the write handles for the
    humidity/temperature sensor, to switch it on/off and functions to
    read it.
    '''
    def __init__(self, periph, characteristics):
        Sensors.__init__(self, periph)
        self.sensor_name = "humidity"
        self.var_names = ["temperature", "relative humidity", ""]
        self.parse_sensor_service(characteristics, 'aa21', 'aa22', 'aa23')

    def parse_value(self, response_text):
        '''function to convert response text human readable sensor values

        temperature and humidity is stored as unsigned short ints (2
        bytes each) in the little endian order. Further, according the
        TI datasheet the 2 least significant bits of the humidity raw
        value are status bits and need to be cleared.

        :param response_text: The response string sent by sensor with raw data
        '''
        try:
            [raw_temp, raw_humid] = struct.unpack('<HH', response_text)
        except:
            return [0, 0, 0]
        else:
            temperature = -46.85 + 175.72 * (float(raw_temp) / pow(2, 16))
            # clear status bits
            raw_humid = (raw_humid & 0xFFFC)
            humidity = -6 + 125.0*(float(raw_humid) / pow(2, 16))
            return [temperature, humidity, 0]

class Barometer(Sensors):
    '''Barometer object. This class will store the write handles for the
    barometric pressure sensor, to switch it on/off and functions to
    read it.
    '''
    def __init__(self, periph, characteristic_services):
        Sensors.__init__(self, periph)
        self.sensor_name = "barometer"
        self.var_names = ["temperature", "barometric pressure", ""]
        self.parse_sensor_service(characteristic_services, 'aa41', 'aa42',
                                  'aa44')
        self.calibrate_handle = None
        if 'aa43' in [uuids[4:8] for uuids in [service.uuid for
                                               service in
                                               characteristic_services]]:
            # If sensortag is Sensortag 1, the uuid 'aa43' denotes
            # calibration handle
            self.c1 = 0.0
            self.c2 = 0.0
            self.offset_poly = [0, 0, 0]
            self.sens_poly = [0, 0, 0]
            self.calibrate_handle = service.val_handle
            try:
                self.calibrate_vals()
            except:
                raise

    def calibrate_vals(self):
        '''Set up calibration coeffs.

        The coefficients are stored as 4 unsigned 16 bit integers and
        4 signed 16 bit integers (c1,...c8). c1 and c2 are used to
        correct the temperature readings while the rest (along with
        corrected temperature) are used to calculate the calibration
        polynomials for pressure.

        The documentation for method :py:meth:`Barometer.parse_value`
        gives equations for calibration.

        '''
        #get calibration details
        if self.calibrate_handle is None:
            return True
        try:
            self.periph.write_by_handle(self.handles['switch'],
                                        str(bytearray([02])))
            cal_pol = self.periph.read_by_handle(self.calibrate_handle)[0]
            #self.periph.write_by_handle(self.handles['switch'],
            #                            self.sensorOn)
        except RuntimeError:
            return False
        else:
            [c1, c2, c3, c4, c5, c6, c7, c8] = struct.unpack('<HHHHhhhh', cal_pol)
            self.c1 = c1/float(pow(2, 24))
            self.c2 = c2/float(pow(2, 10))

            self.sens_poly = [c3, c4/float(pow(2, 17)),
                              c5/float(pow(2, 34))]
            self.offset_poly = [c6*pow(2, 14), c7/8.0, c8/float(pow(2,
                                                                    19))]
            return True

    def parse_value(self, response_text):
        '''function to convert response text human readable sensor values

        **In case of SensorTag 1:**

        The response text contains temperature (signed 16 bit) and
        pressure (unsigned 16 bit). This function applies the
        calibration corrections to output temperature in Celsius, and
        ambient pressure in hectoPascals (or millibars).

        Formula for temperature from application note, rev_X:
        .. math::

           Ta = ((c1 * Tr) / 2^24) + (c2 / 2^10)

        Formula for pressure from application note, rev_X:
        .. math::

           Sensitivity = (c3 + ((c4 * Tr) / 2^17) + ((c5 * Tr^2) / 2^34))

           Offset = (c6 * 2^14) + ((c7 * Tr) / 2^3) + ((c8 * Tr^2) / 2^19)

           Pa = (Sensitivity * Pr + Offset) / 2^14

        **In case of SensorTag 2:**

        The response text is contains temperature (signed 3 bytes) and
        pressure (unsigned 3 bytes). The data is already precalibrated
        and only needs a scaling factor of 100.
        .. math::

          Temperature = raw_temp/100.00

          Pressure = raw_pressure/100.00

        :argument str response_text: The response string sent by sensor with raw data

        :returns: A list containing Temperature in Celsius, and Pressure in hPa (or millibars)

        :rtype: List

        '''
        if self.calibrate_handle is None:
            # Sensortag 2 handles calibration and conversion itself
            raw_temp = struct.unpack('<i', response_text[:3] + '\x00')[0]
            raw_press = struct.unpack('<I', response_text[3:] + '\x00')[0]
            self.periph.printinfo([raw_temp/100.0, raw_press/100.0, 0])
            return [raw_temp/100.0, raw_press/100.0, 0]
        else:
            (raw_temp, raw_press) = struct.unpack('<hH', response_text)
            temperature = raw_temp*self.c1 + self.c2
            sensitivity = polynomial(self.sens_poly, raw_temp)
            offset = polynomial(self.offset_poly, raw_temp)
            pressure = (raw_press*sensitivity + offset)/(100.0*pow(2, 14))
            return [temperature, pressure, 0]


def polynomial(coeffList, x):
    '''Evaluate polynomial in one variable with value of variable and
    coefficients as inputs.

    '''
    result = 0.0
    for i in range(len(coeffList)):
        result = result + coeffList[i]*pow(x, i)
    return result

class Luxometer(Sensors):
    '''Ambient Light Sensor

    Class that handles all the ambient light sensor handles/uuids.

    '''
    def __init__(self, periph, characteristics):
        Sensors.__init__(self, periph)
        self.sensor_name = "luxometer"
        self.var_names = ["lumens", "", ""]
        self.parse_sensor_service(characteristics, 'aa71', 'aa72',
                                  'aa73')
    def parse_value(self, response_text):
        '''Parse raw luminosity values.

        The response text contains the luminosity values as 2 byte int
        in the little endian order (and scaled by 100). The luminosity
        is obtained by dividing the raw int value by 100.00 to get the
        float lux value.

        :param response_text: The 2 byte text string containing the
         raw value in little endian order.

        :returns: A list containing the luminosity (lux) and two empty
         values (0) to stay similar to the format of the other sensors

        '''
        raw_light = struct.unpack('<H', response_text)[0]
        return [raw_light/100.0, 0, 0]

class Inertial(Sensors):
    '''Inertial sensor that returns gyro, acclero and magnetometer data.

    .. attribute:: grange

      Specifies the range of the sensor measurements. It is a 2 bit
      number (i.e from 0 - 4) and the range of the accelerometer is
      calculated as :math:`\pm 2^{(range+1)} * g` where g is the
      gravitational acceleration (9.81 m/s^2)

    '''
    def __init__(self, periph, characteristics):
        Sensors.__init__(self, periph)
        self.grange = 0
        self.sensorOn = str(bytearray([0b10111000, self.grange]))
        self.sensorOff = str(bytearray([0, 0]))
        self.sensor_name = "inertial"
        self.var_names = ["gyro x , y, z axis",
                          "accls x, y, z axis",
                          "mag x, y, z axis"]
        self.parse_sensor_service(characteristics, 'aa81', 'aa82',
                                  'aa83')

    def parse_value(self, response_text):
        '''Parse text to raw signed values

        The inertial sensor stores each component sensor data as
        signed integer (2 byte length) for each axis. These are in
        little endian format. This function parses the text and then
        calls raw_to_float method for each component sensor to convert
        to actual sensor sensor values.

        :param response_text: 18 byte string containing the following
         format of text:
         **GXL,GXM,GYL,GYM,GZL,GZM,AXL,AXM,AYL,AYM,AZL,AZM,MXL,MXM,MYL,MYM,MZL,MZM**
         where prefix G denotes Gyroscope, A - Accelerometer and M -
         magnetometer and the suffix L denotes LSB and M denotes MSB
        :return: List containing list of component sensor readings along each axis.
        :return_type: List
        '''
        try:
            raw_gyro = struct.unpack('<hhh', response_text[0:6])
            raw_accls = struct.unpack('<hhh', response_text[6:12])
            raw_mag = struct.unpack('<hhh', response_text[12:])
            accls = [self.raw_accls_to_float(raw) for raw in raw_accls]
            gyro = [self.raw_gyro_to_float(raw) for raw in raw_gyro]
            mag = [self.raw_mag_to_float(raw) for raw in raw_mag]
        except ValueError:
            return [0, 0, 0]
        else:
            return [gyro, accls, mag]

    def raw_accls_to_float(self, intVal):
        '''Convert raw values to float.

        The sensor stores :math:`0 - 2^{range+1}g` accelerations as values 0-32768, and
        values :math:`-2^{range+1}g - 0` as values -32767 to 0. This function converts
        the integer value to the correct float value (multiplying by
        the value of g = 9.81 m/s^2)

        :param intVal: The raw integer value obtained from sensor
         (between 0 and 32768)
        :return: The floating value corresponding to acceleration in m/s^2
        '''
        gravity = 9.81
        r_g = pow(2, self.grange + 1) # +/- 2g if range = 0
        half = pow(2,15) # necessary to fix the break-off point for
                         # +ve and -ve readings
        quart = float(half/r_g) # gets the correct unit of reading =
                                # (1/quart)g
        floatVal = (intVal)/quart
        return floatVal*gravity

    def raw_gyro_to_float(self, intVal):
        ''' Convert gyroscope readings to float values

        Mimics the :meth:`Sensors.Gyroscope.raw_to_float` method.
        '''
        f_val = intVal*(500.0)/pow(2, 16)
        return f_val

    def raw_mag_to_float(self, intvalue):
        '''Convert integer value to float depending on sensor range

        Mimics the :meth:`Sensors.Magnetometer.raw_to_float` method.

        '''
        f_val = intvalue*(2000.0)/pow(2, 16)
        return f_val

class KeyPress(Sensors):
    '''Class to send key press notifications

    The buttons on the Sensortag send notifications, and these are
    read by this class. The magnet sensor (hall effect) also acts as
    button with a value 4.
    '''
    def __init__(self, periph, characteristics):
        Sensors.__init__(self, periph)
        self.sensor_name = "keystate"
        self.var_names = ["button 1", "button 2", "Magnet state"]
        service = characteristics[0]
        if service.uuid[4:8] == 'ffe1':
            if ord(service.properties) & 0b10000:
                self.handles['notify'] = service.val_handle+1
                self.handles['read'] = service.val_handle

    def parse_value(self, response_string):
        ''' Parse string obtained from sensor

        '''
        val = [0, 0, 0]
        raw_val = struct.unpack('B',response_string)[0]
        if raw_val & 0b001:
            val[0] = 1
        if raw_val & 0b010:
            val[1] = 1
        if raw_val & 0b100:
            val[2] = 1
        return val
