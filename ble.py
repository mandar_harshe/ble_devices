# June 2015
#
# Function to test and use BLE devices in python without resorting to
# hacks. Needs pygattlib (available at
# https://bitbucket.org/mandar_harshe/pygattlib), based on the
# pygattlib version provided by Oscar Acena.
#
# This software is under the terms of GPLv3 or later.
#
# Code by Mandar Harshe @ LORIA, Nancy, France
# (mandar.harshe@inria.fr, mandarharshe@gmail.com)
#

from __future__ import print_function
import gattlib
import time

from Sensors import Battery


class Primary(object):
    '''Class that holds a primary service.

    This class holds the start and end handles, the UUID for the
    service and list of all characteristic services within this
    primary service.

    '''
    def __init__(self, start = None, end = None, uuid = None):
        self.start = start
        self.end = end
        self.uuid = uuid
        self.characteristics = None

    def parse_prim_string(self, prim_string):
        pairs = prim_string.split(',')
        for item in pairs:
            pair = item.split(':')
            if pair[0].lower() == 'attr_handle':
                self.start = int(pair[1], 16)
            elif pair[0].lower() == 'end_handle':
                self.end = int(pair[1], 16)
            elif pair[0].lower() == 'uuid':
                self.uuid = pair[1]

class Characteristics(gattlib.GATTResponse, object):
    '''Class that holds all characteristics discovered

    .. attribute:: services

      The list containing all the services discovered for the given
      characteristic.

    .. attribute:: end

      This contains the end handle for the characteristic.

    .. attribute:: status

      This is specifies if all the characteristics services have been
      read or not. Default value is False. Is set to True once all
      services have been discovered.

    '''
    def __init__(self, end_handle):
        super(Characteristics, self).__init__()
        self.services = []
        self.end = end_handle
        self.status = False

    def on_response(self, response_string):
        '''Response function that parses data read after sending a
        discover_characteristics command.

        '''        
        pairs = response_string.split(',')
        serv = Service()
        for item in pairs:
            pair = item.split(':')
            if pair[0].lower() == 'attr_handle':
                serv.attr_handle = int(pair[1], 16)
            elif pair[0].lower() == 'properties':
                serv.properties = pair[1]
            elif pair[0].lower() == 'value handle':
                serv.val_handle = int(pair[1], 16)
            elif pair[0].lower() == 'uuid':
                serv.uuid = pair[1]
        self.services.append(serv)
        if self.end == serv.val_handle:
            # signify that all services in this characteristic service
            # have been saved
            self.status = True

class Service(object):

        ''' Class that defines the service in the device.

    .. attribute::attr_handle

      The handle for attribute description.

    .. attribute::val_handle

      The handle at which the value of the service can be obtained.

    .. attribute::properties

      The properties of the service. 

    '''
    def __init__(self, attr = None, prop = None, val_handle = None, uuid = None):
        self.attr_handle = attr
        self.properties = prop
        self.val_handle = val_handle
        self.uuid = uuid

##########

class BLE_Device(gattlib.GATTRequester, object):
    '''Class for a generic BLE device

    .. attribute:: primary

      List of all primary services

    .. attribute:: mac_address

      MAC Address of the device

    .. attribute:: state

      If correct device is initialized, and connected this state is
      set to True, else it is set to False. Also helps us check
      whether the device connected on initialize or not.

    '''
    def __init__(self, mac_address = None):
        self.primary = []
        self.mac_address = mac_address
        self.state = False
        self.battery = None
        try:
            super(BLE_Device, self).__init__(self.mac_address, False,
                                             "low")
            self.state = True
        except:
            raise

    def service_discovery(self):
        if self.primary == []:
            try:
                self.get_primary()
            except:
                raise
        if not self.is_connected():
            self.connect()
        for primary in self.primary:
            primary.characteristics = Characteristics(primary.end)
            try:
                self.discover_characteristics(primary.characteristics,
                                              primary.start, primary.end)
                time.sleep(0.01)
            except:
                raise

    def get_primary(self):
        '''Get all primary services

        Calls the :py:meth:`GATTRequester.discover_primary` function
        and stores the data as a list in :py:attribute:`primary`

        '''
        try:
            prim = self.discover_primary()
        except:
            raise
        for str_prim in prim:
            prim_obj = Primary()
            prim_obj.parse_prim_string(str_prim)
            self.primary.append(prim_obj)

    def parse_services(self):
        for primary in self.primary:
            if len(primary.characteristics.services) is 0:
                #wait for the characteristics to be received
                self.printerr("Characteristics not received yet")
                time.sleep(5)
            self.parse_service(primary)

    def parse_service(self, primary):
        '''Dummy function that parses each service.

        To be overloaded for each ble device, depending on the
        "sensors" in that device.

        '''
        if primary.uuid.endswith('-0000-1000-8000-00805f9b34fb'):
            self.parse_adopted_service(primary)

    def parse_adopted_service(self, primary):
        '''If known adopted services exist, and modules are defined, create
        those objects.

        If battery service exists, create battery object.

        '''
        if primary.uuid[4:8] == '180f':
            self.battery = Battery(self)

    def connect(self):
        '''Overwriting connect function

        If connection is not established on SensorTag init, then this
        function is called. Since at init the
        :class:`gattlib.GATTRequester` may not be initialized in
        case of runtime errors, we reinitialise it here in that case.

        If, however, the connection was terminated from client, then
        it provides a way to reconnect. This method works well with
        the default :meth:`gattlib.GATTRequester.is_connected`
        method.

        '''
        while self.state is False:
            try:
                super(BLE_Device, self).__init__(self.mac_address, False, "low")
                self.state = True
                self.printinfo("Connected to device:  %s"%self.mac_address)
            except RuntimeError as expt:
                if str(expt) != "Invalid device!":
                    self.printerr("Could not connect. Press button to reset: %s"% expt)
                    time.sleep(2)
                else:
                    raise KeyboardInterrupt("Bluetooth device not active")
        time.sleep(4)
        if not self.is_connected():
            '''Start 2 connection tries. If all fail, raise RuntimeError
            exception

            '''
            for i in range(2):
                try:
                    super(BLE_Device, self).connect(True)
                    break
                except KeyboardInterrupt:
                    self.printerr("Aborting connect")
                    break
                except RuntimeError:
                    if self.is_connected():
                        break
                    else:
                        self.printerr("Could not connect to " + self.mac_address)
                        self.printerr("Retrying after 5 seconds. Ensure that tag is advertising by pressing side button")
                        #print(i)
                        time.sleep(5)
                except:
                    self.printerr("Big error somewhere")
                    raise
                if self.is_connected():
                    '''Connection successful, break from loop

                    '''
                    self.printinfo("Connection successful with %s"%self.mac_address)
                    time.sleep(3)
                    break
        if not self.is_connected():
            raise RuntimeError

    def printerr(self, message):
        '''Print error message. Using this allows us to later override it and
        use the other logging functions if needed

        '''
        print(message)

    def printinfo(self, message):
        '''Print info message. Using this allows us to later override it and
        use the other logging functions if needed

        '''
        print(message)
