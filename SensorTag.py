# January 2015
#
# Function to test and use TI Sensor Tag in python
# without resorting to hacks. Needs pygattlib (available on PyPi at
# https://bitbucket.org/OscarAcena/pygattlib).
#
# This software is under the terms of GPLv3 or later.
#
# Code by Mandar Harshe @ LORIA, Nancy, France
# (mandar.harshe@inria.fr, mandarharshe@gmail.com)
#!/usr/bin/env python
from __future__ import print_function

from .Sensors import Accelerometer, Magnetometer, Luxometer
from .Sensors import Barometer, Humidity, Gyroscope
from .Sensors import  Inertial, KeyPress
from .ble import BLE_Device

import time

class SensorTag(BLE_Device, object):
    '''SensorTag class

    This class inherits from the :class:`gattlib.GATTRequester` of
    the :mod:`gattlib` module

    .. attribute:: mac_address

      The MAC address of the SensorTag device.

    .. attribute:: sensor_list 

      A dictionary of all sensor objects contained in the class. The
      dict is populated by using the :py:meth:`ble.BLE_Device.service_discovery` and
      parse_services methods. The keys for the sensors are "common
      names" for the sensors.

      * accls: Accelerometer
      * baro: Barometer
      * humid: Humidity
      * mag: Magnetometer
      * iner: Inertial Sensor (Sensortag 2 only)
      * but: Buttons and Hall sensor (Sensortag 2 only)
      * temp: Temperature sensor
      * light: Ambient Light Sensor (Sensortag 2 only)

    '''
    def __init__(self, mac_address):
        self.mac_address = mac_address
        super(SensorTag, self).__init__(self.mac_address)
        self.sensor_list = {}

    def reset_sensor(self, sensor):
        '''Reset sensor to disable it and disable notifications'''

        try:
            #self.printinfo("Disabling sensor")
            self.disable_sensor(sensor)
        except:
            raise
        try:
            #self.printinfo("Disabling notifications")
            self.disable_notify_sensor(sensor)
        except RuntimeError:
            pass
        except Exception:
            raise
        
    def enable_sensor(self, sensor):
        '''Enable sensor

        Enable sensor and set status flag as true.

        '''
        try:
            self.printinfo("Enabling sensor " + sensor.sensor_name)
            sensor.status = sensor.enable()
        except Exception as e:
            self.printerr(e)
            raise e
            
    def disable_sensor(self, sensor):
        '''Disable sensor

        Disable sensor and set status flag as False.

        '''
        try:
            sensor.status = sensor.disable()
        except RuntimeError:
            self.disconnect()
            raise
        except:
            raise

    def enable_notify_sensor(self, sensor):
        '''Enable sensor notifications

        Enable sensor and set status flag as true if successful.
        '''
        if not sensor.notify_status:
            try:
                sensor.notify_status = sensor.enable_notify()
            except Exception as e:
                self.printerr(e)
                raise e

    def disable_notify_sensor(self, sensor):
        '''Disable sensor notifications

        Disable sensor and set status flag as false if successful.
        '''
        if sensor.notify_status is not False:
            try:
                sensor.notify_status = sensor.disable_notify()
            except Exception as e:
                self.printerr(e)
                raise e

    def read_battery(self):
        '''Read battery value

        '''
        self.read_by_uuid_async(self.battery.uuid, self.battery)

    def read_sensor(self, sensor):
        '''Read sensor value

        Send request to read sensor value and call the
        :meth:`Sensors.Sensors.on_response()` method
        asynchronously when the response from sensor is received.

        '''
        if not sensor.status:
            self.enable_sensor(sensor)
            time.sleep(3)
        try:
            self.read_by_handle_async(sensor.handles['read'], sensor)
        except Exception as e:
            raise e
                
    def publish(self, sensor):
        '''dummy function. To be overloaded later for publishing.
        '''
        self.printinfo("%s sensor has the values: %s" %(sensor.sensor_name, sensor.vals))

    def on_notification(self, handle, data):
        '''Function to run on receiving notification.

        This function overrides the function defined in the
        :mod:`gattlib` module. Here, it checks which handle has
        generated the notification and based on the sensor which that
        handle corresponds to, calls the parse function.

        Further, after successful parsing, it calls the
        :meth:`SensorTag.publish` method to publish the sensor
        value. This :meth:`SensorTag.publish` method may be overwritten to
        perform additional functions (local storage etc)

        '''
        for sensor in self.sensor_list.values():
            if format(sensor.handles['read']) == format(handle):
                #print(len(data))
                if len(data) > 2:
                    try:
                        sensor.vals = sensor.parse_value(data[3:])
                        self.publish(sensor)
                    except Exception as e:
                        self.printerr("Exception in on_notification %s"%e)

    def parse_service(self, primary):
        '''Overriding the parse_service method of the BLE_Device class.

        This function adds to the functionality of the
        :meth:`ble.BLE_Device.parse_service` method , and parses the
        available services to create the Sensor objects that are
        components of the Sensortag device.

        '''
        u = primary.uuid
        if u.endswith('-0000-1000-8000-00805f9b34fb'):
            if u[4:8] == 'ffe0':
                chars = primary.characteristics
                if len(chars.services) is 0:
                    self.printerr("waiting for characteristics")
                    time.sleep(5)
                services = chars.services
                primary.sensor = KeyPress(self, services)
                self.sensor_list['but'] = primary.sensor
            else:
                self.parse_adopted_service(primary)
        elif u[0:4]== "f000":
            #print(primary.__dict__)
            chars = primary.characteristics
            if len(chars.services) is 0:
                time.sleep(5)
            services = chars.services

            if u[4:8] == "aa00":
                # IR Temperature sensor
                pass
            elif u[4:8] == "aa10":
                # Accelerometer for SensorTag 1
                #print(services)
                primary.sensor = Accelerometer(self,
                                               services)
                self.sensor_list['accls'] = primary.sensor
            elif u[4:8] == 'aa20':
                # Humidity sensor for SensorTag 1 and 2
                primary.sensor = Humidity(self,
                                          services)
                self.sensor_list['humid'] = primary.sensor
            elif u[4:8] == 'aa30':
                # Magnetometer for SensorTag 1 only
                primary.sensor = Magnetometer(self,
                                              services)
                self.sensor_list['mag'] = primary.sensor
            elif u[4:8] == 'aa40':
                # Barometer for SensorTag 1 and 2
                primary.sensor = Barometer(self,
                                           services)
                self.sensor_list['baro'] = primary.sensor
            elif u[4:8] == 'aa50':
                # Gyroscope (for Sensortag 1 only)
                primary.sensor = Gyroscope(self,
                                           services)
                self.sensor_list['gyro'] = primary.sensor
            elif u[4:8] == 'aa70':
                #Luxometer/ light sensor
                primary.sensor = Luxometer(self,
                                           services)
                self.sensor_list['light'] = primary.sensor
            elif u[4:8] == 'aa80':
                # Inertial sensor
                primary.sensor = Inertial(self, services)
                self.sensor_list['iner'] = primary.sensor
        else:
            self.printinfo(u)
