# BLE Devices 

This a small set of python objects that use pygattlib. The version
shared here uses the discover_services branch on my fork of [pygattlib](https://bitbucket.org/mandar_harshe/pygattlib).

Also includes classes for the [TI SensorTag (1.0 and 2.0)](http://www.ti.com/sensortag)

## Installation notes ##

Make sure you have pygattlib installed from the discover_services branch of pygattlib. Here's how you do it:

    $ hg clone https://mandar_harshe@bitbucket.org/mandar_harshe/pygattlib
    $ hg pull && hg update discover_services
    $ make
    $ python setup.py install

## Usage ##

Make sure pygattlib is in your environment. Usage for declaring
devices is similar to gattlib.

    from ble import BLE_Device
  
    device = BLE_Device("00:11:22:33:44:55")

## Connection ##

A few bells and whistles added to the connect function from
gattlib. Usage is similar, but without arguments.

    device.connect()

## Service discovery and parsing ##


A recommended method for connecting and using a new device is as
follows:

    from ble import BLE_Device

    device = BLE_Device("00:11:22:33:44:55")
    device.connect()
    if device.is_connected():
        device.service_discovery()
        device.parse_services()

A list of all services will be available in `device.primary`.

### SensorTag

In case of a SensorTag, we can use the class `SensorTag` and the above
set of commands also creates a dictonary containing the sensors in the
device.